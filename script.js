'use strict';
function createCountryElement(y) {
    let div = document.createElement('div');
    const html = `
        <h1>${y.name}</h1>
        <h2>Capital: ${y.capital}</h2>
        <img class=" w-25 img-thumbnail" src="${y.flag} " alt="Post image">
        <p>Сurrency: ${y.currencies[0].name}</p>
        <p>Region: ${y.region}</p>
        <p>Google search: <a href="https://www.google.com/search?q=${y.name}" target="_blank">Click</a></p>
    `
    div.innerHTML += html;
    div.classList.add("m-2", "bd-highlight");
    return div;
}
function addContry(x) {
    let y = document.getElementsByClassName('d-flex')[0];
    y.insertBefore(x, y.childNodes[2]);
}

const searchContryForm = document.getElementById('form');
searchContryForm.addEventListener('submit', getContry); 

async function getContry(e) {
    e.preventDefault();
    let value = document.getElementById("contryInput").value;
    if(value.length<2){
        alert("You should enter 2 symbols min");
        document.getElementById("contryInput").value='';
        $("#contryInput").focus();
        return;
    }else{
    const response = await fetch('https://restcountries.eu/rest/v2/name/'+value);
    if (response.ok) { 
        let json = await response.json();
        addContry(createCountryElement(json[0]));
        document.getElementById("contryInput").value='';
        document.getElementById("contryInput").focus();
    } else {
        alert("Can't find this County! Try again!");
        document.getElementById("contryInput").value='';
        document.getElementById("contryInput").focus();
    }
}
  
}